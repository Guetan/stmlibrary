/*
 * SO1602A_I2C.c
 *
 *  Created on: 2019/02/24
 *      Author: nabeya11
 */

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "so1602a_i2c.h"

float powi(float a, int b) {
	if (b < 0) {
		return 1 / powi(a, -b);
	}
	float product = 1;
	while (b > 0) {
		if ((b & 1) == 1) {
			product *= a;
		}
		b >>= 1;
		a *= a;
	}
	return product;
}

int ipowi(int a, int b) {
	if (b < 0) {
		return 1 / powi(a, -b);
	}
	int product = 1;
	while (b > 0) {
		if ((b & 1) == 1) {
			product *= a;
		}
		b >>= 1;
		a *= a;
	}
	return product;
}

/*
 * SO1602�f�[�^��������
 * @param	data : 1�������̃f�[�^
 * @return
 */
void OLED_WriteData(uint8_t data) {
    HAL_I2C_Mem_Write(&SO1602_I2C_HANDLER, SO1602_I2C_ADDRESS, SO1602_DATA, 1, &data,1,1000);
    HAL_Delay(1);
}

/*
 * Send �\��������
 * @param	p : ������
 * @return
 */
void OLED_Print(const char *p) {
    HAL_I2C_Mem_Write(&SO1602_I2C_HANDLER, SO1602_I2C_ADDRESS, SO1602_DATA, 1, (uint8_t*)p,strlen(p),100);
    HAL_Delay(1);
}

void OLED_NumabsPrint(int32_t num, int8_t const_digit){
	char num_data[10] = {0};
	int8_t max_digit, digit;

	//2:num=0�̎���log�����Ȃ�
	if(num == 0){
		for(uint8_t i = 0; i < const_digit; i++){
			num_data[i] = '0';
		}
	}
	else{
		digit = log10f(num) + 1;
		//3:�\���������f
		if(digit < const_digit){
			digit = const_digit;
		}
		for(max_digit = digit; digit >= 1; digit--){
			num %= ipowi(10, digit);
			num_data[max_digit - digit] = num / ipowi(10, (digit - 1)) + '0';
		}
	}
	OLED_Print(num_data);
}

void OLED_NumPrint(int32_t num, uint8_t const_digit){
	//1:�����\��
	if(num < 0){
		OLED_WriteData('-');
		num = -num;
	}
	else{
		OLED_WriteData(' ');
	}
	OLED_NumabsPrint(num, const_digit);
}

void OLED_NumfPrint(float fnum, uint8_t int_digit, uint8_t dec_digit){//float�^(-����)
	float dec_part, int_part;

	dec_part = modff(fnum, &int_part);
	OLED_NumPrint(int_part, int_digit);
	OLED_WriteData('.');
	dec_part *= ipowi(10, dec_digit);
	OLED_NumabsPrint(dec_part, dec_digit);
}

/*
 * Send Command
 * @param cmd �R�}���hso1602_cmd_t����I��
 * @return
 */
void OLED_SendCommand(so1602_cmd_t cmd) {
    HAL_I2C_Mem_Write(&SO1602_I2C_HANDLER, SO1602_I2C_ADDRESS, SO1602_COMMAND, 1, &cmd, 1, 0xffff);
    HAL_Delay(1);
}

/*
 * Ddram�Ƀf�[�^���Z�b�g(�����֐�)
 * @param	p : ������
 * @return
 */
void OLED_SetDdramAddress(uint8_t daddr) {
	OLED_SendCommand(SO1602_CMD_SET_DDRAM_ADDRESS | daddr);
	HAL_Delay(37);
}

void OLED_SendEntryMode(so1602_entrymode_t entry_type, so1602_entrystate_t entry_states) {
	static uint8_t entry_mode;
	switch(entry_states){
		case SCROLL_ON:
		case RIGHT_TO_LEFT:
			entry_mode &= ~entry_type;
			break;

		case SCROLL_OFF:
		case LEFT_TO_RIGHT:
			entry_mode |=  entry_type;
			break;

		default:
			return;
			break;
	}
	OLED_SendCommand(SO1602_CMD_SET_ENTRY_MODE | entry_mode);
	HAL_Delay(37);
}

/*
 * Send �f�B�X�v���C���[�h
 * @param	display_type so1602_dispmode_t����I�� �f�B�X�v���C�̋@�\���
 *          displaystates so1602_dispstate_t����I���@���̋@�\�̃I���I�t
 * @return
 */
void OLED_SendDisplayMode(so1602_dispmode_t disp_type, so1602_dispstate_t disp_states) {
	static uint8_t display_mode=0x00;
	switch(disp_states){
		case DISPLAY_ON:
			display_mode |=  disp_type;
			break;
		case DISPLAY_OFF:
			display_mode &= ~disp_type;
			break;
		default:
			return;
			break;
	}
	OLED_SendCommand(SO1602_CMD_SET_DISPLAY_MODE | display_mode);
	HAL_Delay(37);
}

void OLED_SendFunctionMode(uint8_t function_mode) {
	OLED_SendCommand(SO1602_CMD_FUNCTION_SET | function_mode);
	HAL_Delay(37);
}

void OLED_EnableSd() {
	OLED_SendCommand(SO1602_CMD_OLED_ENABLE_SD);
}

void OLED_DisableSd() {
	OLED_SendCommand(SO1602_CMD_OLED_DISABLE_SD);
}

/*
 * SO1608�����ݒ�
 * @param
 * @return
 */
void OLED_Init(void) {
	HAL_Delay(100);
	OLED_Display(DISPLAY_OFF);	// Display OFF
	HAL_Delay(2);
	OLED_Clear();				// Clear Display
	HAL_Delay(20);
	OLED_Home();				// Return Home
	HAL_Delay(2);
	OLED_Display(DISPLAY_ON);	// Display ON
	HAL_Delay(150);
	OLED_Clear();				// Clear Display
	HAL_Delay(20);
	OLED_FadeOutAndBlinking(SO1602_DISABLE_FADE_OUT_AND_BLINKING,0);
}

void OLED_Clear(){
	OLED_SendCommand(SO1602_CMD_CLEAR_DISP);	// Clear Display
}

void OLED_Home(){
	OLED_SendCommand(SO1602_CMD_RETURN_HOME);	// Return Home
}

void OLED_Display(so1602_dispstate_t disp_states){
	OLED_SendDisplayMode(SO1602_DISPMODE_DISPLAY, disp_states);
}

void OLED_Cursor(so1602_dispstate_t disp_states){
	OLED_SendDisplayMode(SO1602_DISPMODE_CURSOR, disp_states);
}

void OLED_Blink(so1602_dispstate_t disp_states){
	OLED_SendDisplayMode(SO1602_DISPMODE_BLINK, disp_states);
}

/*
 * move cursor
 * @param	row �s
 *          column ��(��������)
 * @return
 */
void OLED_SetCursor(uint8_t row, uint8_t column) {
	uint8_t row_begin[] = {0, 32};
	if(row>0 && row<=2)
		OLED_SetDdramAddress((column - 1) + row_begin[row-1] );
}

void OLED_Shift(so1602_shiftdir_t shift_dir){
	OLED_SendCommand(SO1602_CMD_CURSOR_OR_DISPLAY_SHIFT | shift_dir);
}

void OLED_FadeOutAndBlinking(uint8_t mode, uint8_t interval) {
	static uint8_t function_mode;
	OLED_SendFunctionMode(function_mode | SO1602_FUNCTION_EXT_RE_ON);
	OLED_EnableSd();
	OLED_SendCommand(SO1602_CMD_SET_FADEOUT_AND_BLINKING);
	OLED_SendCommand(mode | (interval & SO1602_FADE_OUT_INTERVAL_MASK));
	HAL_Delay(37);
	OLED_DisableSd();
	OLED_SendFunctionMode(function_mode & SO1602_FUNCTION_EXT_RE_OFF);
}


# Encoder Library

## Overview

ロータリーエンコーダを使った位置・速度計算のライブラリ

タイマ割り込みと併用する

## CubeMX setting

### エンコーダ用タイマの設定
使用するタイマのCombined Channelsを`Encoder Mode`にする
ConfigurationのPrescalerは0
CounterPeriodを65535に設定する
Encoder Modeを `Encoder TI1 and TI2`　にする

### 制御用タイマ割り込みの設定
エンコーダとは別のタイマで割り込みをする
- ActivatedないしはInternal Clockをチェックする
- ConfigurationのPrescalerとCounterPeriodを適切に設定して、割り込み周期を設定する(個人的に10msがおすすめ)
cycle = 1/freq
freq = APBxTimerclocks / (Prescaler+1) / (CounterPeriod+1)
Prescaler, CounterPeriodの最大サイズ(bit数)に注意
- NVIC Settingsの`TIMxGlobal interrpt`をチェックする

## Initialize

1. `Enc_HandleTypedef`構造体を宣言
1. 構造体の各項目を設定
1. `EncoderEnable()`の引数に構造体をセット
1. 無限ループ前に制御タイマ割り込み開始

#### `Enc_HandleTypedef`構造体内訳
- Enc_InitTypedef Init;
初期化時の設定項目 次項で説明
- int32_t prev_cnt;
前回のカウント値。ユーザーは触らない
- float pos;
位置 読み出し・書き込み可
- float vel;
速度 読み出しのみ可

#### `Enc_InitTypedef`構造体内訳
- TIM_HandleTypeDef* htim;
使用するタイマのハンドラ
- float update_freq;
割り込み間隔(s)
- Enc_FW cnt_dir;
正カウント方向 `ENC_FW_CNTUP` or `ENC_FW_CNTDOWN` を代入
- float value_per_pulse;
1パルス当たりの進度(mm/pulse, m/pulse, rad/pulse ,deg/pulseなどなど)
除算演算子前の数値には`f`をお忘れなく
ex. 1024PPRのエンコーダ一回転で10mm進むとき
`value_per_pulse= 10.0f / 1024;`

## Update Encoder Data

タイマ割り込み内で`EncoderUpdateData`を実行する

## Get Encoder Data

`__ENC_GET_POSITION()`及び`__ENC_GET_VELOCITY()`マクロを利用する
引数は対象の`Enc_HandleTypedef`構造体
構造体encから直接enc.posと読むこともできるが`__ENC_GET_POSITION(enc)`とラップすることが望ましい

## Set Position

位置に関しては任意のタイミングで指定することができる
__ENC_GET_POSITION(__ENC_STRUCT__, __VAULE__)

- __ENC_STRUCT__ :対象のエンコーダの`Enc_HandleTypedef`構造体
- __VAULE__ :設定する値

## Code Example
エンコーダを二つ読むプログラム

### 想定

タイマ割り込み TIM6を使用、10ms周期

encoder1
- タイマはTIM2
- 1024PPR, 直径60mmのタイヤ
- ENC_FW_CNTUPが正方向だった
- 速度が読みたい

encoder2
- タイマはTIM3
- 600PPR, radianで、ギアが回転対象:エンコーダ = 7:6 (対象一回転でエンコーダ軸は6/7回転)
- ENC_FW_CNTDOWNが正方向だった
- 初期位置は 90°(Pi/2)

注：`M_PI`はmath.hに定義される円周率の値です。他に`M_TWOPI`(2π)　や `M_PI_2`(π/2)などがあります

uartとprintfを使用して値を表示しています。
~~printfでのuart出力や、float出力の方法などは他を参照してください~~

```c

//---some code from CubeMX---//

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h> //for printf
#include <math.h>
#include "encoder.h"
/* USER CODE END Includes */

//---some code from CubeMX---//

/* USER CODE BEGIN PV */
Enc_HandleTypedef enc1;
Enc_HandleTypedef enc2;
/* USER CODE END PV */

//---some code from CubeMX---//

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//for printf
int __io_putchar(int ch)
{
	HAL_UART_Transmit(&huart3, (uint8_t*) &ch, 1, 500);
	return ch;
}

//timer interrupt
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim->Instance == TIM6){
		static int print_cnt=0;

		//Update encoder data
		EncoderUpdateData(&enc1);
		EncoderUpdateData(&enc2);

		//print values 
		if(print_cnt>3){ //print per 30ms
			printf("enc1 vel:%f\tenc2 vel:%f\n\r", __ENC_GET_VELOCITY(&enc1), __ENC_GET_POSITION(&enc2));
			print_cnt=0;
		}
		else{
			print_cnt++;
		}
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

	//----Some Init from CubeMX----//

	/* USER CODE BEGIN 2 */
	//encoder1 Initialize
	enc1.Init.htim = &htim2;
	enc1.Init.update_freq = 0.01f;
	enc1.Init.cnt_dir = ENC_FW_CNTUP;
	enc1.Init.value_per_pulse = M_PI * 60f / 1024;
	EncoderEnable(&enc1);

	//encoder2 Initialize
	enc2.Init.htim = &htim3;
	enc2.Init.update_freq = 0.01f;
	enc2.Init.cnt_dir = ENC_FW_CNTDOWN;
	enc2.Init.value_per_pulse = M_PI*0.060f / 600;
	EncoderEnable(&enc2);

	//encoder2 pos set
	__ENC_SET_POSITION(&enc2, M_PI/2);

	//start ctrl timer
	HAL_TIM_Base_Start_IT(&htim6);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while(1)
	{
	/* USER CODE END WHILE */

	/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

//---some code from CubeMX---//
```
/*
 * AQM0802A.h
 *
 *  Created on: 2018/12/08
 *      Author: Ryohei
 */

#ifndef AQM0802A_H_
#define AQM0802A_H_

#include <math.h>
#include <stdlib.h>
#include "main.h"

//I2Cハンドラ
#define AQM0802A_I2C_HANDLER		hi2c2

//LCDアドレス
#define AQM0802A_ADDRESS			0x7C

//プロトタイプ宣言
void AQM0802AInit(void);

#endif /* LCD_H_ */

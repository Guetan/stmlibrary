/*
 * AQM0802A.c
 *
 *  Created on: 2018/12/08
 *      Author: Ryohei
 */

#include "AQM0802A.h"

void AQM0802AInit(void) {
	const uint8_t tx_init_data[9] = {0x38, 0x39, 0x14, 0x70, 0x56, 0x6C, 0x38, 0x0C, 0x01};
	HAL_I2C_Master_Transmit(&AQM0802A_I2C_HANDLER, AQM0802A_ADDRESS, (uint8_t*)tx_init_data, 9, 1000);
}

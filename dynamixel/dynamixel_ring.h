/*
 * dynamixel_ring.h
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#ifndef DYNAMIXEL_DYNAMIXEL_RING_H_
#define DYNAMIXEL_DYNAMIXEL_RING_H_

#include "main.h"

#define RING_SUCCESS 0
#define RING_FAIL 1
#define DYNAMIXEL_RING_BUF_SIZE 512

struct dynamixel_ring_buf {
  uint8_t buf[DYNAMIXEL_RING_BUF_SIZE];
  uint16_t buf_size;
  uint16_t w_ptr, r_ptr;
  uint16_t overwrite_cnt;
  UART_HandleTypeDef *huart;
};

void dynamixel_ring_init(struct dynamixel_ring_buf *ring, UART_HandleTypeDef *huart);
int dynamixel_ring_getc(struct dynamixel_ring_buf *ring, uint8_t *c);
int dynamixel_ring_putc(struct dynamixel_ring_buf *ring, uint8_t c);
int dynamixel_ring_available(struct dynamixel_ring_buf *ring);

#endif /* DYNAMIXEL_DYNAMIXEL_RING_H_ */

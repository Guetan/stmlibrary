#include "main.h"
#include "fxas21002.h"
#include <stdio.h>
#include <math.h>

#define DEG2RAD(x)  (float)((x) * M_PI / 180)

#define FXAS21002_ADR (0x21<<1)

#define FXAS21002_SENSITIVITY_250DPS  (0.0078125F) // Table 35 of datasheet
#define FXAS21002_SENSITIVITY_500DPS  (0.015625F)
#define FXAS21002_SENSITIVITY_1000DPS (0.03125F)
#define FXAS21002_SENSITIVITY_2000DPS (0.0625F)

// 初期化時にこの回数だけ測定する
#define FXAS21002_INIT_LOOP_NUM 1000

#define FXAS21002_TIMEOUT 300

// センサとの通信用
static I2C_HandleTypeDef *phi2c;
// ゼロ点補正
volatile static fxas21002_data_t offset;
// ジャイロのレンジ
static float fxas21002_sensitivity;

// プロトタイプ宣言
static uint8_t fxas21002Read(uint8_t reg);
static void fxas21002Write(uint8_t reg, uint8_t val);
static void fxas21002GetRawValue(fxas21002_data_t *ret);

/*
 * データ読み込み
 * @param reg レジスタアドレス
 * @return 読み込んだデータ
 */
static uint8_t fxas21002Read(uint8_t reg) {
    uint8_t data;
    HAL_I2C_Mem_Read(phi2c, FXAS21002_ADR, reg, 1, &data, 1, FXAS21002_TIMEOUT);
    return data;
}

/*
 * データ書き込み
 * @param reg: レジスタアドレス
 *        val: 書き込む値
 */
static void fxas21002Write(uint8_t reg, uint8_t val) {
    HAL_I2C_Mem_Write(phi2c, FXAS21002_ADR, reg, 1, &val, 1, FXAS21002_TIMEOUT);
    return;
}

/*
 * 初期化処理
 * @param hi2c: i2c handler
 *        range: センサのレンジ
 * @note rangeは250, 500, 1000, 2000 DPS から選ぶ
 */
void fxas21002Init(I2C_HandleTypeDef *hi2c, fxas21002_range_t range) {
    uint8_t ctlreg0;
    phi2c = hi2c;
	//WHO_AM_I
	uint8_t gyro_check;
	do{
		gyro_check = fxas21002Read(FXAS21002_REGISTER_WHO_AM_I);
		printf("FXAS21002 : Who am I = %d\n\r", gyro_check);
		HAL_Delay(1);
	}while(gyro_check != 0xD7);

	switch (range) {
        case FXAS21002_RANGE_250DPS:
            ctlreg0 = 0x03;
            fxas21002_sensitivity = FXAS21002_SENSITIVITY_250DPS;
            break;
        case FXAS21002_RANGE_500DPS:
            ctlreg0 = 0x02;
            fxas21002_sensitivity = FXAS21002_SENSITIVITY_500DPS;
            break;
        case FXAS21002_RANGE_1000DPS:
            ctlreg0 = 0x01;
            fxas21002_sensitivity = FXAS21002_SENSITIVITY_1000DPS;
            break;
        case FXAS21002_RANGE_2000DPS:
            ctlreg0 = 0x00;
            fxas21002_sensitivity = FXAS21002_SENSITIVITY_2000DPS;
            break;
    }
    fxas21002Write(FXAS21002_REGISTER_CTRL_REG1, (1<<6));    // reset
	HAL_Delay(1);
    fxas21002Write(FXAS21002_REGISTER_CTRL_REG1, 0x00);      // standby
    fxas21002Write(FXAS21002_REGISTER_CTRL_REG0, ctlreg0);   // set Gyro range 
    fxas21002Write(FXAS21002_REGISTER_CTRL_REG1, 0x0a);      // activate
	HAL_Delay(60);

    printf("Range:%#x\n\r", fxas21002Read(FXAS21002_REGISTER_CTRL_REG0));

    for (uint16_t i=0; i<FXAS21002_INIT_LOOP_NUM; i++) {
        do{
    		gyro_check = fxas21002Read(FXAS21002_REGISTER_STATUS) & 0b00001000;
			HAL_Delay(1);
        }while(gyro_check);
        fxas21002_data_t val;
        fxas21002GetRawValue(&val);
        offset.pitch += val.pitch / FXAS21002_INIT_LOOP_NUM;
        offset.roll  += val.roll / FXAS21002_INIT_LOOP_NUM;
        offset.yaw   += val.yaw / FXAS21002_INIT_LOOP_NUM;
    }
    printf("Offset:%f %f %f\n\r", offset.pitch, offset.roll, offset.yaw);
}

/*
 * yaw角加速度
 */
float fxas21002GetYawAngVel() {
    uint8_t dat[2] = {};
    HAL_I2C_Mem_Read(phi2c, FXAS21002_ADR, FXAS21002_REGISTER_OUT_Z_MSB,
                     1, dat, 2, FXAS21002_TIMEOUT);
    return (float)DEG2RAD((int16_t)(dat[0] << 8 | dat[1]) * fxas21002_sensitivity) - offset.yaw;
}

/*
 * 各角速度の"測定値"
 * @note オフセットを引く前の値
 */
static void fxas21002GetRawValue(fxas21002_data_t *ret) {
    uint8_t dat[7] = {};
    HAL_I2C_Mem_Read(phi2c, FXAS21002_ADR, 0x00 | 0x80, 1, dat, 7, FXAS21002_TIMEOUT);
    int16_t x_val = (int16_t)((dat[1] << 8) | dat[2]),  
            y_val = (int16_t)((dat[3] << 8) | dat[4]), 
            z_val = (int16_t)((dat[5] << 8) | dat[6]);
    ret->pitch = DEG2RAD(x_val * fxas21002_sensitivity);
    ret->roll = DEG2RAD(y_val * fxas21002_sensitivity);
    ret->yaw = DEG2RAD(z_val * fxas21002_sensitivity);
    return;
}

/*
 * 各軸の角加速度
 * @param ret: pitch, roll, yawの値をまとめた構造体
 */
void fxas21002GetAngVel(fxas21002_data_t *ret) {
    fxas21002GetRawValue(ret);
    ret->pitch -= offset.pitch;
    ret->roll -= offset.roll;
    ret->yaw -= offset.yaw;
    return;
}

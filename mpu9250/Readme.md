# MPU9250 Library

## Overview

MPU9250の角速度を読むライブラリ
加速度・地磁気は未対応

## CubeMX setting

### SPIの設定
使用するSPIを`Full-Duplex Master`にする
Configulationで以下の設定をする
First Bit = MSB First
Clock Polarity = Low
Clock Phase = 2 Edge

### CSピンの設定
SPIのチップセレクト：CS(スレーブセレクト：SSとも)をGPIO_Outputで指定する。UserLavel推奨

## Library settings
mpu9250.hにて

GYRO_CS_PORT GYRO_CS_PINを設定する

他のデバイスと共用で同じバスを使う場合、SHARE_SPI_BUSをdefineします。
これはSPImodeが他デバイスと異なるときの対処法です

## Initialize

1. `MPU9250Init(SPI_HandleTypeDef* hspi)`を呼ぶ。hspiはCubeが吐いたSPIのハンドラ構造体のポインタ
1. `MPU9250GetAngVelOffset()`を呼び、オフセットを設定する。この際センサは揺らしてはならない

## Read

`MPU9250GetYaw()`でYaw軸が読めます。
他の二軸は`MPU9250GetAngvel()`で実装されているように見えますが、`MPU9250GetAngVelOffset()`でoffsetが取られていないので修正する必要があります

## Code Example
Yaw軸の角速度と角度を算出

### 想定

使用するのはSPI3

TIM6を使い10ms周期で割り込み
角速度を加算して角度にする

```c

//---some code from CubeMX---//

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>			//for printf
#include "mpu9250.h"
/* USER CODE END Includes */

//---some code from CubeMX---//

/* USER CODE BEGIN PV */
float yaw_angvel;
float yaw_angle=0;
/* USER CODE END PV */

//---some code from CubeMX---//

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//for printf
int __io_putchar(int ch)
{
	HAL_UART_Transmit(&huart3, (uint8_t*) &ch, 1, 500);
	return ch;
}

//timer interrupt
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim->Instance == TIM6){
		static int print_cnt=0;

		//Update angvel data
		yaw_angvel = MPU9250GetYaw();
		yaw_angle += yaw_angvel;

		//print values 
		if(print_cnt>3){ //print per 30ms
			printf("angvel:%f\tangle:%f\n\r", yaw_angvel, yaw_angle);
			print_cnt=0;
		}
		else{
			print_cnt++;
		}
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

	//----Some Init from CubeMX----//

	/* USER CODE BEGIN 2 */
	MPU9250Init(&spi3);
	MPU9250GetAngVelOffset();

	HAL_TIM_Base_Start_IT(&htim6);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while(1)
	{
	/* USER CODE END WHILE */

	/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}
```
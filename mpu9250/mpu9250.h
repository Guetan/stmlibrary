/*
 * mpu9250.h
 *
 *  Created on: 2018/08/15
 *      Author: Ryohei
 */

#ifndef MPU9250_H_
#define MPU9250_H_

#include "main.h"

// #define SHARE_SPI_BUS

//CSポート、ピン
#define GYRO_CS_PORT				GPIOD
#define GYRO_CS_PIN					GPIO_PIN_2

typedef struct{
	float pitch;
	float roll;
	float yaw;
}mpu9250_struct;

/* Exported functions prototypes ---------------------------------------------*/
void MPU9250Init(SPI_HandleTypeDef*);
void MPU9250GetAngVelOffset(void);

float MPU9250GetYaw(void);


#endif /* MPU9250_H_ */

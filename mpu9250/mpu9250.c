/*
 * mup9250.c
 *
 *  Created on: 2018/08/15
 *      Author: Ryohei
 */

#include <stdio.h>
#include <math.h>
#include "mpu9250.h"
#include "light_math.h"

//レジスタ
#define MPU9250_WHO_AM_I			0x75
#define MPU9250_SMPLRT_DIV			0x19
#define MPU9250_CONFIG				0x1A
#define MPU9250_GYRO_CONFIG			0x1B
#define MPU9250_ACCEL_CONFIG		0x1C
#define MPU9250_ACCEL_CONFIG_2		0x1D
#define MPU9250_INT_PIN_CFG			0x37
#define MPU9250_ACCEL_XOUT_H		0x3B
#define MPU9250_ACCEL_XOUT_L		0x3C
#define MPU9250_ACCEL_YOUT_H		0x3D
#define MPU9250_ACCEL_YOUT_L		0x3E
#define MPU9250_ACCEL_ZOUT_H		0x3F
#define MPU9250_ACCEL_ZOUT_L		0x40
#define MPU9250_GYRO_XOUT_H			0x43
#define MPU9250_GYRO_XOUT_L			0x44
#define MPU9250_GYRO_YOUT_H			0x45
#define MPU9250_GYRO_YOUT_L			0x46
#define MPU9250_GYRO_ZOUT_H			0x47
#define MPU9250_GYRO_ZOUT_L			0x48
#define MPU9250_PWR_MGMT_1      	0x6B
#define MPU9250_PWR_MGMT_2      	0x6C

//ファイル内グローバル変数
volatile static float angvel_offset = 0;
static SPI_HandleTypeDef* mpu_hspi;

//static関数プロトタイプ宣言
static int8_t MPU9250InitSub(void);
static void MPU9250Send(uint8_t, uint8_t);
static uint8_t MPU9250SingleRead(uint8_t);
static void MPU9250MultiRead(uint8_t, uin8_t*, uint8_t);
static void MPU9250Select(void);
static void MPU9250Deselect(void);
static void MPU9250Wait(void);

/*
 * ジャイロZ軸入力
 * @param
 * @return	角速度(rad/s)
 */
float MPU9250GetYaw(void) {
	float angvel;
	volatile uint8_t data_h, data_l;
	volatile int16_t data;

#ifdef SHARE_SPI_BUS
	//通信設定変更
	mpu_spi->Init.FirstBit = SPI_FIRSTBIT_MSB;
	HAL_SPI_Init(mpu_spi);
#endif

	data_h = MPU9250SingleRead(MPU9250_GYRO_ZOUT_H);
	data_l = MPU9250SingleRead(MPU9250_GYRO_ZOUT_L);
	data = (data_h << 8) + data_l;

	if (data >= 0) angvel = (DEG2RAD(data * 0.06103516f) - angvel_offset) * 1.0;
	else           angvel = (DEG2RAD(data * 0.06103516f) - angvel_offset) * 1.0;
	return angvel;
}

/*
 * ジャイロ全軸読み取り
 * @param	angvel 角速度(rad/s)が返るポインタ
 * @return	
 */
void MPU9250GetAngvel(mpu9250_struct* angvel){
	uint8_t rxdata[6+1];
	int16_t data[3];

#ifdef SHARE_SPI_BUS
	//通信設定変更
	mpu_spi->Init.FirstBit = SPI_FIRSTBIT_MSB;
	HAL_SPI_Init(mpu_spi);
#endif

	MPU9250MultiRead(MPU9250_GYRO_XOUT_H, rxdata, 6);

	for(uint8_t i = 0; i < 3; i+=2){
		data[i] = (rxdata[i+1] << 8) + rxdata[i+2];
	}

	//変換(生値で正回転・逆回転の特性差を調整する)
	if (data >= 0) angvel->pitch = (DEG2RAD(data[0] * 0.06103516f) - angvel_offset) * 1.0;
	else           angvel->pitch = (DEG2RAD(data[0] * 0.06103516f) - angvel_offset) * 1.0;
	if (data >= 0) angvel->roll = (DEG2RAD(data[1] * 0.06103516f) - angvel_offset) * 1.0;
	else           angvel->roll = (DEG2RAD(data[1] * 0.06103516f) - angvel_offset) * 1.0;
	if (data >= 0) angvel->yaw = (DEG2RAD(data[2] * 0.06103516f) - angvel_offset) * 1.0;
	else           angvel->yaw = (DEG2RAD(data[2] * 0.06103516f) - angvel_offset) * 1.0;
}

/*
 * 角速度のゼロ点を測定
 * @param
 * @return
 */
void MPU9250GetAngVelOffset(void) {
	volatile float angvel_sum = 0;
	const uint16_t num_of_sample = 10000;
	angvel_offset = 0;

	//開始
	printf("MPU9250 : Measuring Angular Velocity Offset...\n\r");

	HAL_Delay(1000);

	//num_of_sample回の測定の平均をとる
	for (int32_t i = 0; i < num_of_sample; i++) {
		angvel_sum += MPU9250GetAngVel();
		MPU9250Wait();
	}
	angvel_offset = angvel_sum / num_of_sample;

	//表示
	printf("MPU9250 : Angular Velocity Offset = %f\n\r", angvel_offset);
}

/*
 * 初期設定複数回呼び出し
 * @param
 * @return
 * @note	電源投入直後は不安定になる可能性があるため
 * @note SPImode
 * SPI_FIRSTBIT_MSB
 * SPI_PHASE_2EDGE
 */
void MPU9250Init(SPI_HandleTypeDef* hspi) {
	volatile int8_t check;
	volatile int8_t count = 0;
	mpu_hspi = hspi;
	printf("MPU9250 : Initializing...\n\r");
	do{
		check = MPU9250InitSub();
		count++;
		if (count >= 10) {
			printf("MPU9250 : Communication Error\n\r");
			return;
		}
	} while (check);
	printf("MPU9250 : Communication Succeeded\n\r");
}

/*
 * 初期設定
 * @param
 * @return
 */
static int8_t MPU9250InitSub(void) {
#ifdef SHARE_SPI_BUS
	//通信設定変更
	mpu_spi->Init.FirstBit = SPI_FIRSTBIT_MSB;
	HAL_SPI_Init(mpu_spi);
#endif

	//WHO_AM_I
	uint8_t gyro_check;
	gyro_check = MPU9250SingleRead(MPU9250_WHO_AM_I);
	printf("MPU9250 : Who am I = %d\n\r", gyro_check);

	//設定
	MPU9250Send(MPU9250_PWR_MGMT_1,  0x00);
	MPU9250Send(MPU9250_PWR_MGMT_2,  0b111110);		//ジャイロZ軸のみENABLE
	MPU9250Send(MPU9250_INT_PIN_CFG, 0x02);
	MPU9250Send(MPU9250_GYRO_CONFIG, 0x18);    		//Full Scale : 2000dps

	if (gyro_check != 0x71){
		return 1;
	} else {
		return 0;
	}
}

/*
 * データ書き込み
 * @param	address : MPU9250レジスタアドレス
 * 			data : 書き込むデータ
 * @return
 */
static void MPU9250Send(uint8_t address, uint8_t data) {
	uint8_t txdata[2] = {address, data};
	uint8_t rxdata[2] = {};
	MPU9250Select();
	HAL_SPI_TransmitReceive(mpu_hspi, &txdata[0], &rxdata[0], 1, HAL_MAX_DELAY);
	HAL_SPI_TransmitReceive(mpu_hspi, &txdata[1], &rxdata[1], 1, HAL_MAX_DELAY);
	MPU9250Deselect();
}

/*
 * データ読み込み
 * @param	address : MPU9250レジスタアドレス
 * @return	読み込んだデータ
 */
static uint8_t MPU9250SingleRead(uint8_t address) {
	uint8_t txdata[2] = {(address | 0x80), 0x00};
	uint8_t rxdata[2] = {};
	MPU9250Select();
	HAL_SPI_TransmitReceive(mpu_hspi, &txdata[0], &rxdata[0], 1, HAL_MAX_DELAY);
	HAL_SPI_TransmitReceive(mpu_hspi, &txdata[1], &rxdata[1], 1, HAL_MAX_DELAY);
	MPU9250Deselect();
	return rxdata[1];
}

/*
 * データ複数読み込み
 * @param	address : MPU9250レジスタアドレス
 * 			rxdata : データrxdata[0]は無効データ
 * 			addr_size : 読むbyte数
 * @return
 */
static void MPU9250MultiRead(uint8_t address, uin8_t* rxdata, uint8_t addr_size) {
	uint8_t txdata[8+1] = {(address | 0x80), 0x00};
	MPU9250Select();
	HAL_SPI_TransmitReceive(mpu_hspi, txdata, rxdata, addr_size+1, HAL_MAX_DELAY);
	MPU9250Deselect();
}

/*
 * チップセレクト
 * @param
 * @return
 */
static void MPU9250Select(void) {
	HAL_GPIO_WritePin(GYRO_CS_PORT, GYRO_CS_PIN, GPIO_PIN_RESET);
}

/*
 * チップセレクト解除
 * @param
 * @return
 */
static void MPU9250Deselect(void) {
	HAL_GPIO_WritePin(GYRO_CS_PORT, GYRO_CS_PIN, GPIO_PIN_SET);
}

/*
 * オフセット時の待ち
 * @param
 * @return
 */
static void MPU9250Wait(void) {
	for (volatile int32_t i = 0; i < 100; i++) ;
}

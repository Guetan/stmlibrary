/*
 * RCservo.h
 *
 *  Created on: 2019/08/18
 *      Author: nabeya11
 */

#ifndef RCSERVO_H_
#define RCSERVO_H_

#include "stm32f4xx_hal.h"

typedef struct{
	TIM_HandleTypeDef* htim;
	uint32_t channel;

	uint16_t center_pos;
}struct_servo;

void RCservo_Init(struct_servo* servo_h);
void RCservo_RadCtrl(struct_servo* servo_h, float rad);
void RCservo_DegCtrl(struct_servo* servo_h, float deg);

#endif /* RCSERVO_H_ */

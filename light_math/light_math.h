/*
 * light_math.h
 *
 *  Created on: 2019/02/27
 *      Author: nabeya11
 */

#ifndef LIGHT_MATH_H_
#define LIGHT_MATH_H_

#include <math.h>

#define RAD2DEG(x)	(float)((x) * 180 / M_PI)
#define DEG2RAD(x)	(float)((x) * M_PI / 180)

#define MAX(a, b)	(((a) > (b)) ? (a) : (b))
#define MIN(a, b)	(((a) < (b)) ? (a) : (b))

float limitf(float var, float ref){
	float abs_ref = fabsf(ref);
	return fmaxf(fminf(var, abs_ref), -abs_ref);
}

#endif /* LIGHT_MATH_H_ */

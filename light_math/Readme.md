# light_math Library

## Overview

`math.h`にない簡単な演算系

## Detail

### Macros

#### RAD2DEG(x)

radian単位のデータxをdegree単位に変換

#### DEG2RAD(x)

degree単位のデータxをradian単位に変換

#### MAX(a,b)

a,bのうち大きい方を返す
整数型の実装はmath.hにない

#### MIN

a,bのうち小さい方を返す
整数型の実装はmath.hにない

### Function

#### limitf(float var, float ref)

varを -ref~refの間に抑え込む
-ref未満は-ref、ref以上はrefを返す
float 実装
/*
 * A3921_motor.h
 *
 *  Created on: Nov 24, 2019
 *      Author: nabeya11
 */

#ifndef A3921_MOTOR_H_
#define A3921_MOTOR_H_

#include "main.h"

#define A3921_DIR_FW	0
#define A3921_DIR_BC		1

typedef struct{
	TIM_HandleTypeDef*	timer;	//timer構造体
	uint32_t			channel;	//timerチャンネル
	uint8_t direction;	//正回転の方向
	GPIO_TypeDef*	phase_port;	//GPIO_Port
	uint16_t		phase_pin;	//GPIO_Pin
}A3921_HandleTypedef;

/* Exported functions prototypes ---------------------------------------------*/
void A3921_motor_init(A3921_HandleTypedef *a3921_struct);
void A3921_Run(A3921_HandleTypedef *a3921_struct, float speed);

#endif /* A3921_MOTOR_H_ */

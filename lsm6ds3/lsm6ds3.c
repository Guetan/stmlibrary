/*
 * LSM6DS3.c
 *
 *  Created on: 2019/05/31
 *      Author: Ryohei
 */

#include "lsm6ds3.h"
#include "light_math.h"
#include <math.h>
#include <stdio.h>

#define GRAVITATIONAL_ACC	9.807f

//ファイル内グローバル変数
volatile static imu_t offset = {};
volatile static float acc_resolution = 0.000488 * GRAVITATIONAL_ACC;
volatile static float gyro_resolution = 0.070f;

//static関数プロトタイプ宣言
static void LSM6DS3Send(uint8_t, uint8_t);
static uint8_t LSM6DS3Read(uint8_t);
static void LSM6DS3Select(void);
static void LSM6DS3Deselect(void);
static void LSM6DS3OffsetWait(void);

/*
 * 6軸データ取得
 * @param
 * @return
 */
void LSM6DS3GetData(imu_t *imu) {
	volatile uint8_t data_h[7] = {}, data_l[7] = {};
	volatile int16_t data[7] = {};

	LSM6DS3_SPI_HANDLER.Init.FirstBit = SPI_FIRSTBIT_MSB;
	LSM6DS3_SPI_HANDLER.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
	HAL_SPI_Init(&LSM6DS3_SPI_HANDLER);

	//6軸+温度読み込み
	for (volatile int8_t i = 0; i < 7; i++) {
		data_l[i] = LSM6DS3Read(LSM6DS3_ACC_GYRO_OUT_TEMP_L + (2 * i));
		data_h[i] = LSM6DS3Read(LSM6DS3_ACC_GYRO_OUT_TEMP_L + 1 + (2 * i));
		data[i] = (data_h[i] << 8) + data_l[i];
		//待ち入れる意味あんのか
		for (volatile int32_t j = 0; j < 10; j++) ;
	}

	//温度
	imu->temp = data[0];

	//角速度
	imu->gyro_x = DEG2RAD(data[1] * gyro_resolution) - offset.gyro_x;
	imu->gyro_y = DEG2RAD(data[2] * gyro_resolution) - offset.gyro_y;
	imu->gyro_z = DEG2RAD(data[3] * gyro_resolution) - offset.gyro_z;

	//加速度
	imu->acc_x = (data[4] * acc_resolution);
	imu->acc_y = (data[5] * acc_resolution);
	imu->acc_z = (data[6] * acc_resolution);
}

/*
 * 6軸のゼロ点を測定
 * @param
 * @return
 */
void LSM6DS3GetOffset(void) {
	imu_t data = {};
	imu_t sum = {};
	const uint16_t num_of_sample = 7500;

	//リセット
	offset.acc_x = offset.acc_y = offset.acc_z = 0;
	offset.gyro_x = offset.gyro_y = offset.gyro_z = 0;

	//num_of_sample回の測定の平均をとる
	for (int32_t i = 0; i < num_of_sample; i++) {
		LSM6DS3GetData(&data);
		sum.acc_x  += data.acc_x;
		sum.acc_y  += data.acc_y;
		sum.acc_z  += data.acc_z;
		sum.gyro_x += data.gyro_x;
		sum.gyro_y += data.gyro_y;
		sum.gyro_z += data.gyro_z;
		LSM6DS3OffsetWait();
	}
	//平均
	offset.acc_x  = sum.acc_x / num_of_sample;
	offset.acc_y  = sum.acc_y / num_of_sample;
	offset.acc_z  = sum.acc_z / num_of_sample;
	offset.gyro_x = sum.gyro_x / num_of_sample;
	offset.gyro_y = sum.gyro_y / num_of_sample;
	offset.gyro_z = sum.gyro_z / num_of_sample;

	//表示
	printf("LSM6DS3 : Offset Acquired\n");
}

/*
 * 初期設定
 * @param
 * @return	imu_status_t
 * @note	設定項目を引数で渡せる様にすべし
 */
imu_status_t LSM6DS3Init(void) {
	const uint8_t who_am_i_correct = 0x69;
	uint8_t who_am_i, check_repeat_num = 5;

	LSM6DS3_SPI_HANDLER.Init.FirstBit = SPI_FIRSTBIT_MSB;
	LSM6DS3_SPI_HANDLER.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
	HAL_SPI_Init(&LSM6DS3_SPI_HANDLER);

	//電源投入直後はWHO_AM_Iが読めないことがあるので複数回読み出す
	do {
		//WHO_AM_I Check
		who_am_i = LSM6DS3Read(LSM6DS3_ACC_GYRO_WHO_AM_I_REG);
		printf("LSM6DS3 : Who am I = %d\n", who_am_i);
		//繰り返し回数更新
		check_repeat_num--;
		//待ち
		HAL_Delay(1);
	} while (check_repeat_num > 0 && who_am_i != who_am_i_correct);
	//WHO_AM_I判定
	if (who_am_i != who_am_i_correct) {					//エラー
		printf("LSM6DS3 : Communication Failed\n");
		return IMU_HW_ERROR;
	} else {
		printf("LSM6DS3 : Communication Succeeded\n");
	}

	//Setting
	LSM6DS3Send(LSM6DS3_ACC_GYRO_CTRL1_XL, LSM6DS3_ACC_GYRO_BW_XL_50Hz | LSM6DS3_ACC_GYRO_FS_XL_16g | LSM6DS3_ACC_GYRO_ODR_XL_26Hz);
	HAL_Delay(1);
	LSM6DS3Send(LSM6DS3_ACC_GYRO_CTRL2_G,  LSM6DS3_ACC_GYRO_FS_125_DISABLED | LSM6DS3_ACC_GYRO_FS_G_2000dps | LSM6DS3_ACC_GYRO_ODR_G_26Hz);
	HAL_Delay(1);
	LSM6DS3Send(LSM6DS3_ACC_GYRO_CTRL8_XL, 0b10010000);

	return IMU_SUCCESS;
}


/*
 * データ書き込み
 * @param	address : LSM6DS3レジスタアドレス
 * 			data : 書き込むデータ
 * @return
 */
static void LSM6DS3Send(uint8_t address, uint8_t data) {
	uint8_t txdata[2] = {address, data};
	uint8_t rxdata[2] = {};
	LSM6DS3Select();
	HAL_SPI_TransmitReceive(&LSM6DS3_SPI_HANDLER, &txdata[0], &rxdata[0], 1, 1);
	HAL_SPI_TransmitReceive(&LSM6DS3_SPI_HANDLER, &txdata[1], &rxdata[1], 1, 1);
	LSM6DS3Deselect();
}

/*
 * データ読み込み
 * @param	address : LSM6DS3レジスタアドレス
 * @return	読み込んだデータ
 */
static uint8_t LSM6DS3Read(uint8_t address) {
	uint8_t txdata[2] = {(address | 0x80), 0x00};
	uint8_t rxdata[2] = {};
	LSM6DS3Select();
	HAL_SPI_TransmitReceive(&LSM6DS3_SPI_HANDLER, &txdata[0], &rxdata[0], 1, 1);
	HAL_SPI_TransmitReceive(&LSM6DS3_SPI_HANDLER, &txdata[1], &rxdata[1], 1, 1);
	LSM6DS3Deselect();
	return rxdata[1];
}

/*
 * チップセレクト
 * @param
 * @return
 */
static void LSM6DS3Select(void) {
	HAL_GPIO_WritePin(LSM6DS3_CS_PORT, LSM6DS3_CS_PIN, GPIO_PIN_RESET);
}

/*
 * チップセレクト解除
 * @param
 * @return
 */
static void LSM6DS3Deselect(void) {
	HAL_GPIO_WritePin(LSM6DS3_CS_PORT, LSM6DS3_CS_PIN, GPIO_PIN_SET);
}

/*
 * オフセット用適当wait
 * @param
 * @return
 */
static void LSM6DS3OffsetWait(void) {
	volatile int32_t t = 750;
	while (t--);
}
